                            Project Description

This “face-detection” project takes your image through the laptop camera and detect the face by drawing a rectangle over the face. It is implemented using the ROS environment.
Here the Haar Cascades algorithm is used. Initially, the algorithm needs a lot of positive images (images of faces) and negative images (images without faces) to train the classifier.
Then we need to extract features from it. 

Installing ROS Kinetic on python 2.7 :

  ROS Kinetic only supports Wily (Ubuntu 15.10), Xenial (Ubuntu 16.04)
and Jessie (Debian 8) for debian packages.
http://wiki.ros.org/kinetic/

  Installing OpenCV
To install OpenCV just follow the steps
https://www.pyimagesearch.com/

  Install cv_bridge 
    Describes how to interface ROS and OpenCV by converting ROS images into OpenCV images, and vice versa.

follow the steps

*sudo apt-get install ros-(ROS version name)-cv-bridge
*sudo apt-get install ros-(ROS version name)-vision-opencv

  Writting simple publisher and subscriber (python)

follow the steps given in the tutorial :
http://wiki.ros.org/ROS/



  To see the output just go through the link

https://www.youtube.com/watch?v=1Qi9MVmjZSw


