import cv2
from cv_bridge import CvBridge, CvBridgeError
import rospy
from sensor_msgs.msg import Image

#face detection function
def face_detect():

 while True:
   #Creating publisher node
    pub = rospy.Publisher("frames", Image, queue_size=10)
    rospy.init_node('frame_pub', anonymous=True)
    rate = rospy.Rate(10) # 1 Hz
        
    # Capture frame-by-frame
    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=2,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE

    )

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Display the resulting frame
    cv2.imshow('Video', frame)

    #give the frames to ros Environment 
    bridge= CvBridge()    
    ros_image = bridge.cv2_to_imgmsg(frame, "bgr8")
        
        
    ## publishes the image with detected faces 
    pub.publish(ros_image)
    rate.sleep()

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
 video_capture.release()
 cv2.destroyAllWindows()


if __name__ == '__main__':

#path of haarcascade xml file for face detection
       faceCascade = cv2.CascadeClassifier("/home/ambadi/opencv-3.1.0/data/haarcascades/haarcascade_frontalface_default.xml")
       #start capturing video
       video_capture = cv2.VideoCapture(0)

       try:
          #calling function face detection
          face_detect()
          
       except rospy.ROSInterruptException:
               pass

